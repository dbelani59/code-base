<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webservices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('webservices/Webservices_model','serve');
    }
	public function login(){
//		echo file_get_contents('php://input');die; 
		$data = json_decode(file_get_contents('php://input'));
		$userdata = $this->serve->login($data);
		if($userdata){
			$rdata['userdata'] = $userdata;
			$rdata['message'] = 'Login Successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'Login Fail!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	
	public function change_status($id){
		$userdata = $this->serve->change_status($id);
		echo "You are verified Now! So, now you can login in into the app properly.";
	}
	
	public function registration(){
		$data = json_decode(file_get_contents('php://input'),true);
		$userdata = $this->serve->registration($data);
		if($userdata != 0){
			$rdata['userdata'] = $userdata;
			$rdata['message'] = 'Registration Successfully! We have sent a confirmation email';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'Email Is already used';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	function create_image(){
		echo $this->serve->create_image();	
	}
	
	public function candidate_list($id){
		$candidate_list = $this->serve->candidate_list($id);
		if($candidate_list != 0){
			$rdata['hire_id'] = $candidate_list['hire_ids'];
			$rdata['userdata'] = $candidate_list['result'];
			$rdata['message'] = 'Candidatelist Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'No Candidate';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	public function land_on_app(){
		$data = json_decode(file_get_contents('php://input'));
		$userdata = $this->serve->login_exist($data);
		$rdata['userdata'] = NULL;
		$rdata['message'] = 'Device ID updated';
		$rdata['code'] = '200';
		echo json_encode($rdata);
	}
	
	public function applied_job_list($id,$lang = 1){
		$jobdata = $this->serve->applied_list($id,$lang);
		if($jobdata){
			$rdata['userdata'] = $jobdata;
			$rdata['message'] = 'Appplied Job List!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'No Jobs';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	public function job_list($lang = 1){
		$employee_list = $this->serve->job_list($lang);
		if($employee_list != 0){
			$rdata['userdata'] = $employee_list;
			$rdata['message'] = 'Job List Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'No Jobs';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);	
	}
	
	public function search_candidate(){
		$data = json_decode(file_get_contents('php://input'));
		$candidate_list = $this->serve->search_candidate_list($data);
		if($candidate_list){
			$rdata['userdata'] = $candidate_list;
			$rdata['message'] = 'Candidatelist Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'No Candidate found';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);	
	}
	
	public function search_employer(){
		$data = json_decode(file_get_contents('php://input'));
		$candidate_list = $this->serve->search_employee_list($data);
		if($candidate_list){
			$rdata['userdata'] = $candidate_list;
			$rdata['message'] = 'Employerlist Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'No Employers found';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);	
	}
	
	public function create_jobs(){
		$data = json_decode(file_get_contents('php://input'),true);
		$userdata = $this->serve->create_jobs($data);
		if($userdata != 0){
			$rdata['jobdata'] = $userdata;
			$rdata['message'] = 'Job Created Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'Sorry, Something is wrong';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	public function my_jobs(){
		$data = json_decode(file_get_contents('php://input'));
		$jobs_list = $this->serve->find_jobs($data);
		foreach($jobs_list as $job):
			$job_data['id'] = $job->id;
			$job_data['employer_id'] = $job->employer_id;
			$job_data['employer_name'] = $job->user_name;
			$job_data['company_name'] = $job->company_name;
			$job_data['contact_number'] = $job->contact_number;
			$job_data['shortlisted_id'] = $job->shortlisted_id;
			$job_data['applied_id'] = $job->user_id;
			$job_data['for_which_position'] = $job->for_which_position;
			$job_data['category'] = $job->category;
			$job_data['category_name'] = $job->category_name;
			$job_data['job_location'] = $job->job_location;
			$job_data['location_lat'] = $job->location_lat;
			$job_data['location_long'] = $job->location_long;
			$job_data['job_description'] = $job->job_description;
			$job_data['experience'] = $job->experience;
			$job_data['status'] = $job->status;
			$job_data['job_date'] = $job->job_date;
			$job_data['created_date'] = $job->created_date;
			$job_data['vacancy'] = $job->no_of_vacancies;
			$job_data['apply_users'] = $this->serve->apply_candidate($job->user_id, $job->id);
			$job_data['shortlist_users'] = $this->serve->shortlist_candidate($job->shortlisted_id);
			$jobs[] = $job_data;
		endforeach;
		if(isset($jobs)){
			$rdata['jobdata'] = $jobs;
			$rdata['message'] = 'Find Jobs Successfull!';
			$rdata['code'] = '200';
		}else{
			$rdata['jobs_list'] = NULL;
			$rdata['message'] = 'No Jobs found';
			$rdata['code'] = '400';
		}
		unset($jobs);
		echo json_encode($rdata);
	}
	
	public function apply_job(){
		$data = json_decode(file_get_contents('php://input'));
		$data_array = json_decode(file_get_contents('php://input'),true);
		$apply_job = $this->serve->apply_job($data);
		if($apply_job == 1){
			$get_name = $this->serve->user_name($data);
			$get_job_name = $this->serve->job_name($data->jobid);
			$regId = $this->serve->get_deviceid_by_job($data->jobid);
			$notification = array();
			$arrNotification= array();			
			$arrData = array();		
			
			$arrNotification = array(
				"body" => $get_name." have applied for ".$get_job_name." job",
				"title" => "Applied job"
			);
			$result = $this->serve->send_notification($regId, $arrNotification,"Android");
			$msg = $get_name." have applied for ".$get_job_name." job";
			$notify = $this->serve->notification($data,$msg,1);
			
			if($apply_job == 1){
				$rdata['jobdata'] = $apply_job;
				$rdata['message'] = 'Job Apply Successfully!';
				$rdata['code'] = '200';
			}else{
				$rdata['jobdata'] = NULL;
				$rdata['message'] = 'Job Apply Fail!';
				$rdata['code'] = '400';
			}
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'You can not apply for this job because vacancy was full!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	public function shortlisted_candidate(){
		$data = json_decode(file_get_contents('php://input'));
		$apply_job = $this->serve->shortlist($data);
		if($apply_job == 1){
			$get_name = $this->serve->user_name($data);
			$get_job_name = $this->serve->job_name($data->jobid);
			$regId = $this->serve->get_deviceid_by_userid($data);
			$notification = array();
			$arrNotification= array();			
			$arrData = array();		
			
			$arrNotification = array(
				"body" => $get_name." you are shorlisted for ".$get_job_name." job",
				"title" => "Applied job"
			);
			$result = $this->serve->send_notification($regId, $arrNotification,"Android");
			
			$msg = $get_name." you are shorlisted for ".$get_job_name." job";
			$notify = $this->serve->notification($data,$msg,2);
			
			$rdata['jobdata'] = $apply_job;
			$rdata['message'] = 'Shortlisted Successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Shortlisted Fail!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);	
	}
	
	public function send_notification_by_jobid($id){
		$regId = $id;
		$notification = array();
		$arrNotification= array();			
		$arrData = array();											
		$arrNotification["body"] ="Test by Vijay.";
		$arrNotification["title"] = "PHP ADVICES";
		$arrNotification["sound"] = "default";
		$arrNotification["type"] = 1;
		$result = $this->serve->send_notification($regId, $arrNotification,"Android");
	}
	
	public function send_notification(){
		$data = json_decode(file_get_contents('php://input'));
		$regId = $this->serve->get_deviceid($data->userid);
		$notification = array();
		$arrNotification= array();			
		$arrData = array();		
		$arrNotification["body"] ="You have applied for this job";
		$arrNotification["title"] = "Applied Job";
		$arrNotification["sound"] = "default";
		$arrNotification["color"] = "#203E78";
		$result = $this->serve->send_notification($regId, $arrNotification,"Android");

	}
	
	public function get_category(){
		$category_list = $this->serve->get_category();
		$data['category'] = $category_list;
		$data['code'] = '200';
		$data['message'] = 'Category List';
		echo json_encode($data);
	}
	
	public function get_profile($id){
		$userdata = $this->serve->get_profile($id);
		if($userdata != "0"){
			$rdata['userdata'] = $userdata;
			$rdata['message'] = 'Profile Detail Successfull!';
			$rdata['code'] = '200';	
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'no profile found';
			$rdata['code'] = '400';				
		}
		echo json_encode($rdata);
	}
	
	public function edit_profile(){
		$data = json_decode(file_get_contents('php://input'),true);
		$userdata = $this->serve->edit_profile($data);
		if($userdata != "0"){
			$rdata['userdata'] = $userdata;
			$rdata['message'] = 'Profile Update Successfully!';
			$rdata['code'] = '200';	
		}else{
			$rdata['userdata'] = NULL;
			$rdata['message'] = 'Email Is already used';
			$rdata['code'] = '400';				
		}
		echo json_encode($rdata);
	}
	
	public function employee_details($id){
		$userdata = $this->serve->employee($id);
		if($userdata){
			$rdata['jobdata'] = $userdata;
			$rdata['message'] = 'Employee detail Successfull!';
			$rdata['code'] = '200';	
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Fail!';
			$rdata['code'] = '400';				
		}
		echo json_encode($rdata);
	}
	
	public function job_details($id){
		$userdata = $this->serve->job($id);
		if($userdata){
			$rdata['jobdata'] = $userdata;
			$rdata['message'] = 'Job Detail Successfull!';
			$rdata['code'] = '200';	
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Fail!';
			$rdata['code'] = '400';				
		}
		echo json_encode($rdata);
	}
	
	public function edit_job(){
		$data = json_decode(file_get_contents('php://input'),true);
		$userdata = $this->serve->edit_job($data);
		if($userdata != "0"){
			$rdata['jobdata'] = $userdata;
			$rdata['message'] = 'Job Update Successfully!';
			$rdata['code'] = '200';	
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Job update fail!';
			$rdata['code'] = '400';				
		}
		echo json_encode($rdata);
	}
	
	public function accept(){
		$data = json_decode(file_get_contents('php://input'));
		$notify_id = $data->notification_id;
		unset($data->notification_id);
		$apply_job = $this->serve->shortlist($data);
		$remove_notfication = $this->serve->remove_notification($notify_id);
		if($apply_job == 1){
			$get_name = $this->serve->user_name($data);
			$get_job_name = $this->serve->job_name($data->jobid);
			$regId = $this->serve->get_deviceid_by_job($data->jobid);
			$notification = array();
			$arrNotification= array();			
			$arrData = array();		
			
			$arrNotification = array(
				"body" => $get_name." accepted for this ".$get_job_name." job.",
				"title" => "Instant hire",
			);
			
			$msg = $get_name." accepted for this ".$get_job_name." job.";
			$notify = $this->serve->notification($data,$msg,4);
			
			$result = $this->serve->send_notification($regId, $arrNotification,"Android");
			$rdata['jobdata'] = $apply_job;
			$rdata['message'] = 'Instant Hire accepted Successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Instant Hire accepted Fail!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);		
	}
	
	public function cancel(){
		$data = json_decode(file_get_contents('php://input'));
		$notify_id = $data->notification_id;
		unset($data->notification_id);
		$apply_job = $this->serve->cancel($data);
		$remove_notfication = $this->serve->remove_notification($notify_id);
		if($apply_job == 1){
			$get_name = $this->serve->user_name($data);
			$get_job_name = $this->serve->job_name($data->jobid);
			$regId = $this->serve->get_deviceid_by_job($data->jobid);
			$notification = array();
			$arrNotification= array();			
			$arrData = array();		
			
			$arrNotification = array(
				"body" => $get_name." not accepted for this ".$get_job_name." job.",
				"title" => "Instant hire"
			);
			
			$msg = $get_name."not accepted for this ".$get_job_name." job.";
			$notify = $this->serve->notification($data,$msg,4);
			
			$result = $this->serve->send_notification($regId, $arrNotification,"Android");
			$rdata['jobdata'] = $apply_job;
			$rdata['message'] = 'Instant Hire Canceled Successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Instant Hire Canceled Fail!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);		
	}
	
	public function instant_hire(){
		$data = json_decode(file_get_contents('php://input'));
		$apply_job = $this->serve->instant_hire($data);
		$notify_data['jobid'] = $data->jobid;
		$notify_data['candidate_id'] = $data->candidate_id;
		if($apply_job == 1){
			$get_name = $this->serve->user_name($data);
			$get_job_name = $this->serve->job_name($data->jobid);
			$regId = $this->serve->get_deviceid_by_userid($data);
			$notification = array();
			$arrNotification= array();			
			$arrData = array();		
			
			$notify = $this->serve->notifications($data,$get_job_name);
			$notify_data['notification_id'] = $notify;
			
			$arrNotification = array(
				"body" => $get_name." you are hire for ".$get_job_name." job. If interested then please accept it.",
				"title" => "Instant hire",
				"data" => json_encode($notify_data)
			);
			
			
			$result = $this->serve->send_notification($regId, $arrNotification,"Android");
			$rdata['jobdata'] = $apply_job;
			$rdata['message'] = 'Instant Hire Successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['jobdata'] = NULL;
			$rdata['message'] = 'Instant Hire Fail!';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);	
	}
	
	function getnotifications($id){
		$notify_data = $this->serve->get_notifications($id);
		if($notify_data){
			$rdata['notifications'] = $notify_data;
			$rdata['message'] = 'Notifications there';
			$rdata['code'] = '200';
		}else{
			$rdata['notifications'] = NULL;
			$rdata['message'] = 'No Notifications';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	function getinstanthire($id){
		$get_data = $this->serve->get_instant_hire($id);
		if($get_data){
			$rdata['userdata'] = $get_data['userdata'];
			$rdata['message'] = 'Instant Hire data successfully!';
			$rdata['code'] = '200';
		}else{
			$rdata['notifications'] = NULL;
			$rdata['message'] = 'No data';
			$rdata['code'] = '400';
		}
		echo json_encode($rdata);
	}
	
	function send_notify($deviceid){
		$arrNotification = array(
			"body" => 'Instant hire demo',
			"title" => "Instant hire"
		);
		$result = $this->serve->send_notification($deviceid, $arrNotification,"Android");	
	}
	
}