"use strict";
var webpack = require('webpack'),
    lessToJs = require('less-vars-to-js'),
    path = require('path'),
    generateIdentifierId = require('./js/helpers/generateIdentifierId'),
    generateVisibilityClasses = require('./js/helpers/generateVisibilityClasses'),
    fs = require('fs'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    // CircularDependencyPlugin = require('circular-dependency-plugin'),
    outputPath = 'H:/xampp/htdocs/wordpress/wp-content/plugins/tatsu/builder/js/',
    extend = function (obj, src) {
        Object.keys(src).forEach(function (key) { obj[key] = src[key]; });
        return obj;
    };

module.exports = function (env) {

    var scheme = null != env && -1 < ['light', 'dark'].indexOf(env.SCHEME) ? env.SCHEME : 'dark',
        customThemeVars = lessToJs(fs.readFileSync(path.join(__dirname, 'less/theme.less'), 'utf8')),
        schemeCSSFile = new ExtractTextPlugin({ filename: 'dark' == scheme ? '../css/dark-scheme.css' : '../css/light-scheme.css' }),
        iframeCSSFile = new ExtractTextPlugin({ filename: 'dark' == scheme ? '../css/dark-context-menu.css' : '../css/light-context-menu.css' });

    if ('dark' == scheme) {
        var customSchemeVars = lessToJs(fs.readFileSync(path.join(__dirname, 'less/darkScheme.less'), 'utf8'));
        customThemeVars = extend(customThemeVars, customSchemeVars);
    }

    var config = {
        entry: [
            'url-search-params-polyfill',
            "./js/root.js"
        ],
        output: {
            filename: "bundle.js"
            // path: '../../../../../xampp/htdocs/wordpress/wp-content/plugins/tatsu/builder/js',
        },
        module: {
            rules: [
                {
                    test: /\.jsx|\.js/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'es2015'],
                        plugins: [
                            ['import', { 'libraryName': 'antd', 'libraryDirectory': 'es', 'style': true }]
                        ]
                    }
                },
                {
                    test: require.resolve("react"),
                    use: "expose-loader?React"
                },
                {
                    test: require.resolve("react-dom"),
                    use: "expose-loader?ReactDOM"
                },
                {
                    test: require.resolve("react-slick"),
                    use: "expose-loader?ReactSlick"
                },
                {
                    test: require.resolve("immutable"),
                    use: "expose-loader?Immutable"
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.less$/,
                    use: schemeCSSFile.extract({
                        use: [
                            {
                                loader: 'css-loader'
                            },
                            {
                                loader: 'less-loader',
                                options: {
                                    modifyVars: customThemeVars
                                }
                            }
                        ]
                    }),
                    exclude: path.join(__dirname, 'js/components/rightPanelComponents/menu/style.less')
                },
                {
                    test: /\.less$/,
                    use: iframeCSSFile.extract({
                        use: [
                            {
                                loader: 'css-loader'
                            },
                            {
                                loader: 'less-loader',
                                options: {
                                    modifyVars: customThemeVars
                                }
                            }
                        ]
                    }),
                    include: path.join(__dirname, 'js/components/rightPanelComponents/menu/style.less')
                }
            ]
        },
        plugins: [
            //  new CircularDependencyPlugin({
            //    exclude : /node_modules/
            //  }),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery"
            }),
            new webpack.ProvidePlugin({
                React: "react"
            }),
            new webpack.ProvidePlugin({
                Immutable: "immutable"
            }),
            new webpack.ProvidePlugin({
                generateIdentifierId: path.resolve(path.join(__dirname, 'js/helpers/generateIdentifierId')),
                generateVisibilityClasses: path.resolve(path.join(__dirname, 'js/helpers/generateVisibilityClasses')),
            }),
            schemeCSSFile,
            iframeCSSFile
        ],
        resolve: {
            alias: {
                'EditorHelpers': path.resolve(__dirname, './js/helpers/editor/ui-utils'),
                'EditorConstants': path.resolve(__dirname, './js/helpers/editor/ui-constants'),
                "@ant-design/icons/lib/dist$": path.resolve(__dirname, './js/helpers/editor/ant-icons')

            }
        }
    };
    if (null != env && env.PROD) {
        config.output.filename = 'bundle.min.js';
        config.plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: true
                }
            })
        );
    } else {
        config.watch = null != env && ['light', 'dark'].indexOf(env.SCHEME) !== -1 ? false : true;
    }
    return config;
}
