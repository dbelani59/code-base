var isEmpty = require('../helpers/emptyCheck');
var TextBlock = React.createClass({

	getStyle: function (atts, textBlock, moduleOptions) {

		var textStyle = {},
			wrapAlignment = atts.get('wrap_alignment');
		//maxWidth = parseValue( textBlock.get( 'name' ), 'max_width', atts.get( 'max_width' ), moduleOptions );
		if (!isEmpty(wrapAlignment)) {
			switch (wrapAlignment) {
				case 'left':
					textStyle.marginLeft = '0';
					break;
				case 'right':
					textStyle.marginLeft = 'auto';
					textStyle.marginRight = '0';
					break;
				case 'center':
					textStyle.marginRight = 'auto';
					textStyle.marginLeft = 'auto';
					break;
				default:
					break;
			}
		}
		// if( 'string' === typeof maxWidth && '' != maxWidth.replace( /\D+/, '' ) && !isNaN( maxWidth.replace( /\D+/, '' ) ) ) {
		// 	textStyle.maxWidth = maxWidth;
		// }else{
		// 	textBlock.maxWidth = '100%';
		// }

		return textStyle;

	},

	getWrapperClass: function (atts) {

		var textClass = 'tatsu-text-inner clearfix ';
		if (!isEmpty(atts.get('scroll_to_animate'))) {
			textClass = textClass + 'scrollToFade ';
		}
		return textClass;

	},

	renderHelper: function (atts, textBlock, moduleOptions) {

		var textClass = this.getWrapperClass(atts),
			cssObject = this.props.cssObject.style,
			moduleOptions = this.props.moduleOptions,
			textBlock = this.props.module,
			dataContent = textBlock.getIn(['atts', 'content']) || '';
			
		return (
				<div className = { textClass } dangerouslySetInnerHTML = { { __html : dataContent } }>
					{/* <style dangerouslySetInnerHTML={{
					__html: `
						.be-pb-observer-${this.props.module.get('id')} .tatsu-text-inner *{
							font-family : ${cssObject['.tatsu-text-inner *'].fontFamily};
							font-weight : ${cssObject['.tatsu-text-inner *'].fontWeight};
							font-size : ${cssObject['.tatsu-text-inner *'].fontSize};
							letter-spacing : ${cssObject['.tatsu-text-inner *'].letterSpacing};
							text-transform:${cssObject['.tatsu-text-inner *'].textTranform};
						}
					`
					}} /> */}	
				</div>
		);


	},

	render: function () {


		var atts = this.props.module.get('atts'),
			deviceVisibility = atts.get('hide_in'),
			moduleOptions = this.props.moduleOptions,
			textBlock = this.props.module,
			cssObject = this.props.cssObject.style,
			textBlockWrapStyle = cssObject['.tatsu-text-block-wrap'];
		// 	visibilityClass = '', 
		// 	visibilityArray;

		// if( !isEmpty( deviceVisibility ) ) {

		// 	visibilityArray = deviceVisibility.split( ',' );
		// 	visibilityArray.forEach( function( device, index ) {
		// 		visibilityClass = visibilityClass + 'tatsu-hide-' + device + ' ';
		// 	} ) 
		// }

		return (
			<div {...generateIdentifierId(atts)} className={'tatsu-module tatsu-text-block-wrap ' + generateVisibilityClasses(atts) + ' ' + atts.get('css_classes')} style={jQuery.extend({}, textBlockWrapStyle, this.props.cssObject.style['root'])}>
				<style dangerouslySetInnerHTML={{
					__html: `
					.be-pb-observer-${this.props.module.get('id')} .tatsu-text-inner *{
						color : ${cssObject['.tatsu-text-inner *'].color}
					}
				`
				}} />
				{
					this.renderHelper(atts, textBlock, moduleOptions)
				}
			</div>
		);
	}

});
module.exports = TextBlock;
